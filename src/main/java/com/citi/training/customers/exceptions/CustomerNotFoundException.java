package com.citi.training.customers.exceptions;

public class CustomerNotFoundException extends RuntimeException {
	
	public CustomerNotFoundException(String errorMessage) {
		super(errorMessage);
	}

}
