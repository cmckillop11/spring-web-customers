package com.citi.training.customers.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.citi.training.customers.exceptions.CustomerNotFoundException;
import com.citi.training.customers.model.Customer;

@Component
public class InMemCustomerDao implements CustomerDao {

	private Map<Integer, Customer> allCustomers = new HashMap<Integer, Customer>();
	
	public void saveCustomer(Customer customer) {
		allCustomers.put(customer.getId(), customer);
	}
	
	public Customer getCustomer(int id) {
		Customer foundCustomer = allCustomers.get(id);
        if(foundCustomer == null) {
        	throw new CustomerNotFoundException("Customer was not found: " + id);
        }
        return foundCustomer;
	}
	
	public List<Customer> getAllCustomers() {
		return new ArrayList<Customer>(allCustomers.values());
	}
	
	public void deleteCustomer(int id) {
		Customer foundCustomer = allCustomers.remove(id);
    	if(foundCustomer == null) {
        	throw new CustomerNotFoundException("Customer was not found: " + id);
        }
	}
}
