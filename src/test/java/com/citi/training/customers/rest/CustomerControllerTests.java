package com.citi.training.customers.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.customers.dao.InMemCustomerDao;
import com.citi.training.customers.model.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CustomerControllerTests {
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InMemCustomerDao mockCustomerDao;

    @Test
    public void findAllCustomers_returnsList() throws Exception { 
    	when(mockCustomerDao.getAllCustomers()).thenReturn(new ArrayList<Customer>());
    	
        MvcResult result = this.mockMvc.perform(get("/customer")).andExpect(status().isOk())
        		.andExpect(jsonPath("$.size()").isNumber()).andReturn();
        
        logger.info("Result from InMemCustomerDao.getAllCustomer: "+ result.getResponse().getContentAsString());
    }
    
    @Test
    public void createCustomer_returnCreated() throws Exception{
    	Customer testCustomer = new Customer(1, "Tester", "123 Hilton Hotel");
    	this.mockMvc.perform(post("/customer").contentType(MediaType.APPLICATION_JSON)
    			.content(objectMapper.writeValueAsString(testCustomer)))
    			.andExpect(status().isCreated()).andReturn();
    	logger.info("Result from Create Customer");
    }
    
    @Test
    public void deleteCustomer_returnsOK() throws Exception {
        MvcResult result = this.mockMvc.perform(delete("/customer/1")).andExpect(status().isNoContent())
                .andReturn();
        logger.info("Result from InMemCustomerDao.delete: " + result.getResponse().getContentAsString());
    }

    @Test
    public void getCustomerById_returnsOK() throws Exception {
    	Customer testCustomer = new Customer(1, "Tester", "123 Hilton Hotel");
        when(mockCustomerDao.getCustomer(testCustomer.getId())).thenReturn(testCustomer);
        MvcResult result = this.mockMvc.perform(get("/customer/1")).andExpect(status().isOk()).andReturn();
        logger.info("Result from InMemCustomerDao.getCustomer: " + result.getResponse().getContentAsString());
    }

}
