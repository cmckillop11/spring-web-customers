package com.citi.training.customers.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.customers.model.Customer;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CustomerControllerIntegrationTests {

	private static Logger logger = LoggerFactory.getLogger(CustomerControllerIntegrationTests.class);
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void getCustomer_returnsCustomer() {
		restTemplate.postForEntity("/customer", new Customer(5, "Bob Smith", "The Hilton Hotel"), Customer.class);
	
		ResponseEntity<Customer> getAllResponse = restTemplate.exchange("/customer/5",
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<Customer>(){});
	
		logger.info("getAllCustomer response: " + getAllResponse.getBody());;
	
		assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
        assertTrue(getAllResponse.getBody().getName().equals("Bob Smith"));
        assertTrue(getAllResponse.getBody().getAddress().equals("The Hilton Hotel"));
	}
	
}
