package com.citi.training.customers.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CustomerTests {
	
	private int testId = 1;
	private String testName = "Tester";
	private String testAddress = "Hilton Hotel, Belfast";
	
	@Test
	public void test_Customer_constructor() {
		Customer testCustomer = new Customer(testId, testName, testAddress);
		
		assertEquals(testId, testCustomer.getId());
        assertEquals(testName, testCustomer.getName());
        assertEquals(testAddress, testCustomer.getAddress());
	}
	
	@Test
    public void test_Customer_toString() {
        String testString = new Customer(testId, testName, testAddress).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testName));
        assertTrue(testString.contains(testAddress));
    }

}
